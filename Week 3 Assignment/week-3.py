import random 

user_pets = [] 

class Pet:
    hunger_threshold= 10
    hunger_decrement= 1
    boredom_threshold = 5
    boredom_decrement = 2
    
    #INITIALIZE ATTRIBUTES
    def __init__(self, name = "Tim", type = "animal"):
        self.__owner = "User" #Private variable
        self.name = name
        self.type = type
        self.hunger = random.randint(0, self.hunger_threshold) #INT
        self.boredom = random.randint(0, self.boredom_threshold) #INT
        self.sounds = [] #LIST
    #INCREMENTS HUNGER AND BOREDOM
    def clock_tick(self):
        self.hunger += 1
        self.boredom += 1
    #CURRENT MOOD OF PET
    def current_mood(self):
        if self.boredom <= self.boredom_threshold and self.hunger <= self.hunger_threshold:
            return "happy"
        elif self.hunger >= self.hunger_threshold:
            return "hungry"
        else:
            return "bored"
    #STR TO DISPLAY CURRENT MOOD OF PET
    def __str__(self) -> str:
        mood = self.current_mood()
        return "\nName- %s, Type- %s, Mood- %s \nHunger- %i, Boredom- %i \nHunger Threshold- %i, Boredom Threshold- %i, Sounds- %s" %(self.name, self.type, mood, self.hunger, self.boredom, self.hunger_threshold, self.boredom_threshold, self.sounds)
    #REDUCE BOREDOM
    def reduce_boredom(self):
        if self.boredom > 0: 
            self.boredom -= self.boredom_decrement
    #REDUCE HUNGER
    def reduce_hunger(self):
        if self.hunger > 0:
            self.hunger -= self.hunger_decrement
    #TEACH WORD METHOD TO REDUCE BOREDOM
    def teach(self, word):
        print(f"\nThank you for teaching me the new word '{word}'")
        self.sounds.append(word)
        self.reduce_boredom()
    #SAY HI TO REDUCE BOREDOM
    def hi(self):
        if len(self.sounds) == 0:
            print(f"\nHello! Teach me a word {pet.__owner}!") #PRIVATE VARIABLE WORKS HERE
        else:
            print(f"\nHi! My favorite word is {random.choice(self.sounds)}")
        self.reduce_boredom()
    #FEED THE PET
    def feed(self):
        print("\nThank you for feeding me!")
        self.hunger -= self.hunger_decrement
        self.reduce_hunger()

#DOG 1 class
class Dog1(Pet):
    def __init__(self, name="Tim", type="dog1"):
        super().__init__(name=name, type=type)
        self.sounds = ["Woof!"]

#DOG 2 class
class Dog2(Pet):
    def __init__(self, name="Tim", type="dog2"):
        super().__init__(name=name, type=type)
        self.sounds = ["Woof Woof!"]

#CAT class
class Cat(Pet):
    def __init__(self, name="Tim", type="cat"):
        super().__init__(name=name, type=type)
        self.sounds = ["Meow!"]

#PUP class
class Pup(Dog2, Dog1):
    def __init__(self, name="Tim", type="pup"):
        super().__init__(name=name, type=type)
        pass
        self.sounds.append("Bow bow!")

def adopt_pet():
    name = input("Enter the name of your pet: ")
    type = input("Enter the type of the pet: ")
    if type == "dog1":
        user_pets.append(Dog1(name, type))
    elif type == "dog2":
        user_pets.append(Dog2(name, type))
    elif type == "cat":
        user_pets.append(Cat(name, type))
    elif type == "pup":
        user_pets.append(Pup(name, type))
    elif len(name) == 0 or len(type) == 0:
        user_pets.append(Pet())
    else:
        user_pets.append(Pet(name, type))

def print_commands():
    if len(user_pets) == 0:
        print("\nYou have no pets! Adopt a new pet!")
        adopt_pet()
    print("\n-----------------------------")
    print("\nYou can: \n0. View all your pets \n1. Adopt a new pet\n OR Interact with existing pets by \n2. Greeting them \n3. Teaching them \n4. Feeding them")    
    print("\n-----------------------------")

def display_user_pets():
    print("****************")
    print("Your Pets")
    for pet in user_pets:
        print(f"\n{pet}")
    print("****************")


#MAIN GAME
game_on = True
while game_on:
    print_commands()
    user_choice = int(input("Enter your choice(0-4):"))
    print(user_choice)
    if user_choice == 0:
        display_user_pets()
    elif user_choice == 1:
        print("\nAdopting a new pet!")
        adopt_pet() 
    elif user_choice in range(2, 5):
        name = input("Enter the name of pet you want to interact with: ")
        pet_exists = False
        for pet in user_pets:
            #print(pet.__owner) PRIVATE VARIABLE WON'T WORK HERE
            if pet.name == name:
                if user_choice == 2:
                    pet.hi()
                elif user_choice == 3:
                    word = input("Enter the word you want to teach: ")
                    pet.teach(word)
                elif user_choice == 4:
                    pet.feed()
                pet_exists = True 
            pet.clock_tick()
        if pet_exists == False:
            print(f"You don't have a pet with the name {name}")
    else:
        continue
